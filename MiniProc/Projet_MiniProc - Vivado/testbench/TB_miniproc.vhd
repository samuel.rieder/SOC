--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   16:02:02 05/02/2018 
-- Design Name:   
-- Module Name:   C:/Work/HEIA-FR/MiniProc/solution/miniproc/testbench/TB_miniproc.vhd
-- Project Name:  miniproc
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: miniproc_top
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
use work.miniproc_pkg.all;
 
ENTITY TB_miniproc IS
END TB_miniproc;
 
ARCHITECTURE behavior OF TB_miniproc IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT miniproc_top
    PORT(
			clk_i   : in std_logic;
			reset_i : in std_logic;
			
			addr_bus_p_o : out std_logic_vector(10 downto 0);
			data_bus_p_o : out std_logic_vector(15 downto 0);
			
			addr_bus_d_o  : out std_logic_vector(10 downto 0);
			data_bus_d_io : inout std_logic_vector(7 downto 0);
		
			rw_o   : out std_logic
        );
    END COMPONENT;

   --Inputs
   signal clk_i : std_logic := '0';
   signal reset_i : std_logic := '0';

	--BiDirs
   signal data_bus_d_io : std_logic_vector(7 downto 0);

 	--Outputs
   signal addr_bus_p_o : std_logic_vector(10 downto 0);
   signal data_bus_p_o : std_logic_vector(15 downto 0);
   signal addr_bus_d_o : std_logic_vector(10 downto 0);
   signal rw_o : std_logic;

   -- Clock period definitions
   constant clk_i_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: miniproc_top PORT MAP (
          clk_i => clk_i,
          reset_i => reset_i,
          addr_bus_p_o => addr_bus_p_o,
          data_bus_p_o => data_bus_p_o,
          addr_bus_d_o => addr_bus_d_o,
          data_bus_d_io => data_bus_d_io,
          rw_o => rw_o
        );

   -- Clock process definitions
   clk_i_process :process
   begin
		clk_i <= '0';
		wait for clk_i_period/2;
		clk_i <= '1';
		wait for clk_i_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	

      wait for clk_i_period*3;
		reset_i <= '1';
		wait for clk_i_period*3;
		reset_i <= '0';

      -- insert stimulus here 

      wait;
   end process;

END;
