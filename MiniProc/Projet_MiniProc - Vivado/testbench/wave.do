onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb_miniproc/uut/clk_i
add wave -noupdate /tb_miniproc/uut/reset_i
add wave -noupdate /tb_miniproc/uut/addr_bus_p_o
add wave -noupdate /tb_miniproc/uut/data_bus_p_o
add wave -noupdate /tb_miniproc/uut/addr_bus_d_o
add wave -noupdate /tb_miniproc/uut/data_bus_d_io
add wave -noupdate -divider Register
add wave -noupdate /tb_miniproc/uut/cmp_proc/cmp_traitement/cmp_registres/reg_s(0)
add wave -noupdate /tb_miniproc/uut/cmp_proc/cmp_traitement/cmp_registres/reg_s(1)
add wave -noupdate /tb_miniproc/uut/cmp_proc/cmp_traitement/cmp_registres/reg_s(2)
add wave -noupdate /tb_miniproc/uut/cmp_proc/cmp_traitement/cmp_registres/reg_s(3)
add wave -noupdate /tb_miniproc/uut/cmp_proc/cmp_traitement/cmp_registres/aw_i
add wave -noupdate /tb_miniproc/uut/cmp_proc/cmp_traitement/cmp_registres/aa_i
add wave -noupdate /tb_miniproc/uut/cmp_proc/cmp_traitement/cmp_registres/ab_i
add wave -noupdate /tb_miniproc/uut/cmp_proc/cmp_traitement/cmp_registres/inreg_i
add wave -noupdate /tb_miniproc/uut/cmp_proc/cmp_traitement/cmp_registres/rw_i
add wave -noupdate /tb_miniproc/uut/cmp_proc/cmp_traitement/cmp_registres/selaa_i
add wave -noupdate /tb_miniproc/uut/cmp_proc/cmp_traitement/cmp_registres/input_reg_s
add wave -noupdate -divider PC
add wave -noupdate /tb_miniproc/uut/cmp_proc/cmp_traitement/cmp_pc/ldpc_i
add wave -noupdate /tb_miniproc/uut/cmp_proc/cmp_traitement/cmp_pc/selpc_i
add wave -noupdate /tb_miniproc/uut/cmp_proc/cmp_traitement/cmp_pc/pc
add wave -noupdate -divider IR
add wave -noupdate /tb_miniproc/uut/cmp_proc/cmp_traitement/cmp_ir/ldir_i
add wave -noupdate -divider Controler
add wave -noupdate /tb_miniproc/uut/cmp_proc/cmp_controler/flag_i
add wave -noupdate /tb_miniproc/uut/cmp_proc/cmp_controler/opcode_i
add wave -noupdate /tb_miniproc/uut/cmp_proc/cmp_controler/state_s
add wave -noupdate -divider {Program memory}
add wave -noupdate /tb_miniproc/uut/cmp_prog_mem/adr_i
add wave -noupdate /tb_miniproc/uut/cmp_prog_mem/data_io
add wave -noupdate /tb_miniproc/uut/cmp_prog_mem/ce_i
add wave -noupdate /tb_miniproc/uut/cmp_prog_mem/rw_i
add wave -noupdate -divider {Data memory}
add wave -noupdate /tb_miniproc/uut/cmp_data_mem/adr_i
add wave -noupdate /tb_miniproc/uut/cmp_data_mem/data_io
add wave -noupdate /tb_miniproc/uut/cmp_data_mem/ce_i
add wave -noupdate /tb_miniproc/uut/cmp_data_mem/rw_i
add wave -noupdate /tb_miniproc/uut/cmp_data_mem/data_mem_s(0)
add wave -noupdate /tb_miniproc/uut/cmp_data_mem/data_mem_s(1)
add wave -noupdate /tb_miniproc/uut/cmp_data_mem/data_mem_s(2)
add wave -noupdate -divider ALU
add wave -noupdate /tb_miniproc/uut/cmp_proc/cmp_traitement/oealu_i
add wave -noupdate /tb_miniproc/uut/cmp_proc/cmp_traitement/cmp_alu/inalu_i
add wave -noupdate /tb_miniproc/uut/cmp_proc/cmp_traitement/cmp_alu/opalu_i
add wave -noupdate /tb_miniproc/uut/cmp_proc/cmp_traitement/cmp_alu/selop_i
add wave -noupdate /tb_miniproc/uut/cmp_proc/cmp_traitement/cmp_alu/ldflag_i
add wave -noupdate /tb_miniproc/uut/cmp_proc/cmp_traitement/cmp_alu/flag_o
add wave -noupdate /tb_miniproc/uut/cmp_proc/cmp_traitement/cmp_alu/a
add wave -noupdate /tb_miniproc/uut/cmp_proc/cmp_traitement/cmp_alu/b
add wave -noupdate /tb_miniproc/uut/cmp_proc/cmp_traitement/cmp_alu/result
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {300 ns} 0}
quietly wave cursor active 1
configure wave -namecolwidth 408
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {1887 ns} {2006 ns}
