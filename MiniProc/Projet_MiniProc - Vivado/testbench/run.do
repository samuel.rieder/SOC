vlib work

vcom -93 ../rtl/miniproc_pkg.vhd
vcom -93 ../rtl/registers.vhd
vcom -93 ../rtl/pc.vhd
vcom -93 ../rtl/ir.vhd
vcom -93 ../rtl/alu.vhd
vcom -93 ../rtl/traitement.vhd
vcom -93 ../rtl/controler.vhd
vcom -93 ../rtl/proc.vhd
vcom -93 ../rtl/data_mem.vhd
vcom -93 ../rtl/prog_mem.vhd
vcom -93 ../rtl/miniproc_top.vhd

vcom -93 TB_miniproc.vhd

vsim -L unisim work.TB_miniproc -voptargs="+acc" 

set NumericStdNoWarnings 1
set StdArithNoWarnings 1

do wave.do
run 2us
view wave

