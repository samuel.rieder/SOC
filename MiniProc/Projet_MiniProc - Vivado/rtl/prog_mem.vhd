----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    14:33:54 11/02/2015 
-- Design Name: 
-- Module Name:    prog_mem - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.miniproc_pkg.all;

entity prog_mem is
	port (
		clk_i : in std_logic;
		adr_i : in std_logic_vector(10 downto 0);
		data_io : inout std_logic_vector(15 downto 0);
		ce_i : in std_logic;
		rw_i : in std_logic
	);
end prog_mem;

architecture Behavioral of prog_mem is

	type prog_mem_type is array(1023 downto 0) of std_logic_vector(15 downto 0);
	--program memory
	signal prog_mem_s : prog_mem_type := ( --    short program:
		0  => "0000000000000000", --       LOAD  R0,M[0]
		1  => "0000100000000001", --       LOAD  R1,M[1]
		2  => "0101001000000000", --       CPY   R2,R1
		3  => "0011000000000010", --       STORE R3,M[2]
		4  => "0001100000000011", --       LOAD  R3,M[3]
		5  => "1110000000000000", --LOOP1  MOD2  R0,R0
		6  => "1010000000000000", --       TEST  R0
		7  => "1000000000000011", --       BR    LOOP2
		8  => "1100000110000000", --       MAC   R0,R3
		9  => "0110111111111100", --       JMP   LOOP1
		10 => "0110000000000000", --LOOP2  KMP   LOOP2
		others => "0000000000000000"
	);
	signal adrint : std_logic_vector(4 downto 0);
	signal outint : std_logic_vector(15 downto 0);
	signal ceint :  std_logic;
	
begin

	adrint <= adr_i(4 downto 0);
	ceint <= '1' when ((ce_i='1') and (31>=to_integer(unsigned(adr_i)))) else '0';
	
	process (clk_i)
	begin
		if falling_edge(clk_i) then
			outint <= prog_mem_s(to_integer(unsigned(adrint)));
			if rw_i=cWRITE then
				if ceint='1' then
					prog_mem_s(to_integer(unsigned(adrint))) <= data_io;
				end if;
			end if;
		end if;
	end process;

	data_io <=  (others => 'Z') when (ceint='1' and rw_i=cWRITE) else outint;
	
end Behavioral;

