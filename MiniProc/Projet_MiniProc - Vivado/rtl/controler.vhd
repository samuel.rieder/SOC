----------------------------------------------------------------------------------
-- Company: 		HEIA FR
-- Engineer: 		Samuel Rieder
-- 
-- Create Date:     10:56:31 05/02/2018 
-- Design Name: 	MiniProc
-- Module Name:     controler - Behavioral 
-- Project Name: 	SOPC
-- Target Devices: 	ZedBoard
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Revision 1.00 - Processor works
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

use work.miniproc_pkg.all;

entity controler is
	port (
		clk_i   : in std_logic;
		reset_i : in std_logic;
		
		flag_i   : in std_logic;
		opcode_i : in std_logic_vector(2 downto 0);
		
		rwmem_o : out std_logic;
		
		selaa_o : out std_logic;
		rwreg_o : out std_logic;
		opalu_o : out std_logic_vector(2 downto 0);
		inalu_o : out std_logic;
		inreg_o : out std_logic;
		oealu_o : out std_logic;
		ldres_o : out std_logic;
		ldpc_o  : out std_logic;
		selpc_o : out std_logic;
		ldir_o  : out std_logic
	);
end controler;

architecture Behavioral of controler is

	--Type
	type ctrl_state is(
		FETCH,DECODE,EXEC,MEM_AC,WRITE_B
	);
	
	--Signal
	signal state: ctrl_state;

begin

	--State Machine process contains the transition between the states
	Synch_State_Decode: process(clk_i, state)
           begin
               if rising_edge(clk_i) then       --synchronisation of the FSM
                   if reset_i = '1' then        --synchronous reset
                       state <= FETCH;
                   else
                       case state is
                           when FETCH =>
                               state <= DECODE;
                           when DECODE =>
                               state <= EXEC;
                           when EXEC =>
                               state <= MEM_AC;
                           when MEM_AC =>
                               state <= WRITE_B;
                           when WRITE_B =>
                               state <= FETCH;
                       end case;
                   end if;
               end if;
        end process;
		
	--Set outputs according to the state of the processor and the opcode
	Output_Decode: process(state, opcode_i, flag_i)
		begin
			--default values
			selaa_o <= '1';
			rwreg_o <= cREAD; --'1'
			inalu_o <= '0';
			inreg_o <= '0';
			oealu_o <= '0';
			ldres_o <= '0';
			ldpc_o  <= '0';
			selpc_o <= '0';
			ldir_o  <= '0';
			rwmem_o <= cREAD; --'1'
			case state is
			   when FETCH =>  --Fetch the instruciton from the IR
					ldir_o <= '1';
			   when DECODE => --Decode the instruction, no action necessary here
					-- do nothing
			   when EXEC =>	  --Execute the instruction, the signals controlling the alu have to be set here
					ldpc_o <= '1';
					ldres_o <= '1';
				   case opcode_i is
						when cCPY_INSTR =>
							selaa_o <= '0';
						when cBR_INSTR =>
							if flag_i = '1' then
								selpc_o <= '1';
							else
								selpc_o <= '0';
							end if;
						when cJMP_INSTR =>
							selpc_o <= '1';                                
						when cMAC_INSTR | cMOD2_INSTR =>
							--selaa_o <= '0';
							inalu_o <= '1';
						when others =>
							-- do nothing
				   end case;
			   when MEM_AC => --Operations which modify the memory do this here
				   case opcode_i is
						when cSTORE_INSTR =>
							rwmem_o <= cWRITE;
							oealu_o <= '1';
						when others =>
							-- do nothing
				   end case;
			   when WRITE_B => --Operations which modify a register do this here
					case opcode_i is
						when cLOAD_INSTR =>
							inreg_o <= '1';
							rwreg_o <= cWRITE;
						when cCPY_INSTR | cMAC_INSTR | cMOD2_INSTR =>
							rwreg_o <= cWRITE;
						when others =>
							-- do nothing
					end case;
			end case;
	end process;
    opalu_o <= opcode_i;
end Behavioral;

