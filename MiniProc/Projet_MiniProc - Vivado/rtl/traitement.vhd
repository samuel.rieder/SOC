----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    14:22:30 05/02/2018 
-- Design Name: 
-- Module Name:    traitement_bloc - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.miniproc_pkg.all;

entity traitement is
	port (
		clk_i   : in std_logic;
		reset_i : in std_logic;
		
		selaa_i : in std_logic;
		rwreg_i : in std_logic;
		opalu_i : in std_logic_vector(2 downto 0);
		inalu_i : in std_logic;
		inreg_i : in std_logic;
		oealu_i : in std_logic;
		ldres_i : in std_logic;
		ldpc_i  : in std_logic;
		selpc_i : in std_logic;
		ldir_i  : in std_logic;
		
		flag_o   : out std_logic;
		opcode_o : out std_logic_vector(2 downto 0);
		
		addr_bus_p_o : out std_logic_vector(10 downto 0);
		data_bus_p_i : in std_logic_vector(15 downto 0);
		
		addr_bus_d_o  : out std_logic_vector(10 downto 0);
		data_bus_d_io : inout std_logic_vector(7 downto 0)
	);
end traitement;

architecture Behavioral of traitement is

	--Signals
	signal ir_s : std_logic_vector(15 downto 0);
	signal pc_s : std_logic_vector(10 downto 0);
	signal alu_s : std_logic_vector(7 downto 0);
	signal carry_s : std_logic;
	signal rega_s, regb_s : std_logic_vector(7 downto 0);
	signal data_in_reg_s : std_logic_vector(7 downto 0);
	
begin

	--Buses and signals connections
	addr_bus_d_o <= ir_s(10 downto 0);
	addr_bus_p_o <= pc_s;
	opcode_o <= ir_s(15 downto 13); 
	

	--Components
	cmp_registres : registers 
	port map (
		clk_i => clk_i, 
		reset_i => reset_i, 
		
		input_bus_i => data_in_reg_s, 
		
		a_o => rega_s,
		b_o => regb_s,
		
		w_i    => ir_s(12 downto 11),
		reg2_i => ir_s(10 downto 9),
		outB_i => ir_s(8 downto 7),
		
		rw_i    => rwreg_i,
		selaa_i   => selaa_i
	);
	
	cmp_alu : alu 
	port map (
		clk_i => clk_i, 
		reset_i => reset_i, 
		
		inalu_i  => inalu_i,
		opalu_i  => opalu_i,
		ldres_i  => ldres_i,
		
		a_i  => rega_s,
		b_i  => regb_s,

		carry_o  => carry_s,
		flag_o   => flag_o,
		result_o => alu_s
	);

	cmp_write_back : write_back
	port map (
		data_i => alu_s,
		
		oealu_i => oealu_i,
		inreg_i => inreg_i,
		
		data2mem_io => data_bus_d_io,
		data2alu_o => data_in_reg_s
	);

	cmp_pc : pc 
	port map (
		clk_i   => clk_i, 
		reset_i => reset_i, 
		
		ldpc_i  => ldpc_i, 
		selpc_i => selpc_i, 
		
		jmp_i => ir_s(11 downto 0), 
		pc_o => pc_s
	);
	
	cmp_ir : ir
	port map (
		clk_i  => clk_i, 
		reset_i => reset_i, 

		ldir_i => ldir_i, 
		
		ir_i => data_bus_p_i, 
		ir_o => ir_s
	);
end Behavioral;

