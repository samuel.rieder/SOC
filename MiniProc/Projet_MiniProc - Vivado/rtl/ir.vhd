----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:54:51 05/02/2018 
-- Design Name: 
-- Module Name:    ir - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.miniproc_pkg.all;

entity ir is
	port (
		clk_i : in std_logic;
		reset_i : in std_logic;
		
		ldir_i : in std_logic;
		ir_i : in std_logic_vector(15 downto 0);
		ir_o : out std_logic_vector(15 downto 0)
	);
end ir;

architecture Behavioral of ir is

begin

	--Code
	process (clk_i)
        begin
           if falling_edge(clk_i) then
               if reset_i='1'  then
                    ir_o <= (others=>'0');
               elsif ldir_i = '1' then
                    ir_o <= ir_i;
               end if;
           end if;
        end process;
        
end Behavioral;

