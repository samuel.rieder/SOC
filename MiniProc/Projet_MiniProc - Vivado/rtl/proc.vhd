----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    14:23:26 05/02/2018 
-- Design Name: 
-- Module Name:    proc - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.miniproc_pkg.all;

entity proc is
	port (
		clk_i   : in std_logic;
		reset_i : in std_logic;
		
		addr_bus_p_o : out std_logic_vector(10 downto 0);
		data_bus_p_i : in std_logic_vector(15 downto 0);
		
		addr_bus_d_o  : out std_logic_vector(10 downto 0);
		data_bus_d_io : inout std_logic_vector(7 downto 0);
		
		rwmem_o  : out std_logic
	);
end proc;

architecture Behavioral of proc is

	--Signals
	signal rwreg_s, selaa_s, inreg_s, ldres_s, inalu_s, oealu_s, ldpc_s, selpc_s, ldir_s, flag_s : std_logic;
	signal opcode_s, opalu_s : std_logic_vector(2 downto 0);
	
begin

	cmp_controler : controler 
	port map (
		clk_i   => clk_i,
		reset_i => reset_i,
		
		flag_i   => flag_s,
		opcode_i => opcode_s,
		
		rwmem_o  => rwmem_o,
		
		rwreg_o  => rwreg_s,
		selaa_o  => selaa_s,
		inreg_o  => inreg_s,
		opalu_o  => opalu_s,
		inalu_o  => inalu_s,
		oealu_o  => oealu_s,
		ldres_o  => ldres_s,
		ldpc_o   => ldpc_s,
		
		selpc_o => selpc_s,
		ldir_o  => ldir_s	
	);
	
	cmp_traitement : traitement 
	port map (
		clk_i   => clk_i,
		reset_i => reset_i,
		
		rwreg_i  => rwreg_s,
		selaa_i  => selaa_s,
		inreg_i  => inreg_s,
		opalu_i  => opalu_s,
		ldres_i  => ldres_s,
		inalu_i  => inalu_s,
		oealu_i  => oealu_s,
		ldpc_i   => ldpc_s,
		selpc_i  => selpc_s,
		ldir_i   => ldir_s,

		flag_o   => flag_s,
		opcode_o => opcode_s,
		
		addr_bus_p_o => addr_bus_p_o, 
		data_bus_p_i => data_bus_p_i,
		
		addr_bus_d_o => addr_bus_d_o, 
		data_bus_d_io => data_bus_d_io
	);
    
    
end Behavioral;