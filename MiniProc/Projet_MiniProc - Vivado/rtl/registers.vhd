----------------------------------------------------------------------------------
-- Company: 		HEIA FR
-- Engineer: 		Samuel Rieder
-- 
-- Create Date:     08:37:23 05/02/2018 
-- Design Name: 	MiniProc
-- Module Name:     registers - Behavioral 
-- Project Name: 	SOPC
-- Target Devices: 	ZedBoard
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Revision 1.00 - Processor works
-- Revision 1.01 - Use Register type instead of several signals
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

use work.miniproc_pkg.all;

entity registers is
	port (
		clk_i   : in std_logic;
		reset_i : in std_logic;
		
		input_bus_i : in std_logic_vector(7 downto 0);
		
		a_o : out std_logic_vector(7 downto 0);
		b_o : out std_logic_vector(7 downto 0);
		
		w_i : in std_logic_vector(1 downto 0);
		reg2_i : in std_logic_vector(1 downto 0);
		outB_i : in std_logic_vector(1 downto 0); 
		
		rw_i  : in std_logic;
		selaa_i : in std_logic         
	);
end registers;

architecture Behavioral of registers is

	type tRegister is array (0 to 3) of  std_logic_vector(input_bus_i'high downto 0);
	
	--Signals
	signal my_register: tRegister;
	signal a_s: std_logic_vector(1 downto 0);
	
begin

    REGISTERS:process (clk_i, reset_i, rw_i)
        begin
           if falling_edge(clk_i) then              --use falling clock, so the registers are definitly ready for the ALU
               if reset_i='1'  then					--reset all registers
                    my_register(0) <= (others=>'0');
                    my_register(1) <= (others=>'0');
                    my_register(2) <= (others=>'0');
                    my_register(3) <= (others=>'0');
               elsif rw_i = cWRITE then
                    my_register(to_integer(unsigned(w_i))) <= input_bus_i;--Write to the specified register
               else
                    a_o <= my_register(to_integer(unsigned(a_s)));      --Read from specified register -> output a
                    b_o <= my_register(to_integer(unsigned(outB_i)));   --Read from specified register -> output b
               end if;
           end if;
    end process;
    a_s <= reg2_i when (selaa_i ='0') else w_i;         --Input MUX
end Behavioral;

