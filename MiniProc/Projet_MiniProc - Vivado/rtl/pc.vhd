----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:59:40 05/02/2018 
-- Design Name: 
-- Module Name:    pc - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.miniproc_pkg.all;

entity pc is
	port (
		clk_i   : in std_logic;
		reset_i : in std_logic;
		
		ldpc_i   : in std_logic;
		selpc_i  : in std_logic;
		
		jmp_i : in std_logic_vector(11 downto 0);
		pc_o : out std_logic_vector(10 downto 0)
	);
end pc;

architecture Behavioral of pc is

	--Signals
	signal pc_i_s, pc_o_s : std_logic_vector(11 downto 0);
	signal offset_pc_s: std_logic_vector(11 downto 0);
	
begin

	--Code
	PC: process(clk_i, reset_i, ldpc_i)
        begin
            if rising_edge(clk_i) then
                if reset_i='1' then
                    pc_o_s        <= (others=>'0');
                elsif ldpc_i = '1' then
                    pc_o_s <= pc_i_s;
                end if;
            end if;
        end process;
    offset_pc_s <= jmp_i when (selpc_i = '1') else "000000000001";
    pc_i_s <= std_logic_vector(signed(pc_o_s)+signed(offset_pc_s));
    pc_o <= pc_o_s(10 downto 0);
end Behavioral;

