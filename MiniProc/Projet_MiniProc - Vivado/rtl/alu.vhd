----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    16:09:36 05/02/2018 
-- Design Name: 
-- Module Name:    alu - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.miniproc_pkg.all;

entity alu is
	port (
		clk_i   : in std_logic;
		reset_i : in std_logic;
		
		inalu_i  : in std_logic;
		opalu_i  : in std_logic_vector(2 downto 0);
		ldres_i  : in std_logic;
		
		a_i      : in std_logic_vector(7 downto 0);
		b_i      : in std_logic_vector(7 downto 0);
		
		carry_o  : out std_logic;
		flag_o   : out std_logic;
		result_o : out std_logic_vector(7 downto 0)
	);
end alu;

architecture Behavioral of alu is

	--Signals
	signal b_s: std_logic_vector(7 downto 0);
    begin

	--Code
    -- MUX on one line
    b_s <= b_i when (inalu_i = '1') else (others => '0');

    REG_ALU: process(clk_i, reset_i, opalu_i)
        begin
           if rising_edge(clk_i) then
               if reset_i='1'  then
                   result_o <= (others=>'0');
                   flag_o <= '0';
               elsif ldres_i = '1' then
                   flag_o <= '0';
                   case opalu_i is
                       when cSTORE_INSTR =>
                           result_o <= a_i;
                       when cCPY_INSTR =>
                           result_o <= a_i;
                       when cTEST_INSTR =>
                           if a_i = b_s then
                               flag_o <= '1';
                           else
                               flag_o <= '0';
                           end if;
                           result_o <= (others=>'0'); 
                       when cMAC_INSTR =>
                           result_o <= std_logic_vector(to_signed(to_integer(signed(a_i))+to_integer(signed(a_i))*to_integer(signed(b_s)), result_o'length));
                       when cMOD2_INSTR =>
                           result_o <= std_logic_vector(to_signed(to_integer(signed(b_i)) mod 2, result_o'length));
                       when others =>
                           result_o <= (others=>'0');
                   end case;
               end if;
           end if;
    end process;
end Behavioral;

