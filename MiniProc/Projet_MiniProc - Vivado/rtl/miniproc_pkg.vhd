--
--	Package File Template
--
--	Purpose: This package defines supplemental types, subtypes, 
--		 constants, and functions 
--
--   To use any of the example code shown below, uncomment the lines and modify as necessary
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package miniproc_pkg is

	--Opcode
	constant cLOAD_INSTR   : std_logic_vector(2 downto 0) := "000";
	constant cSTORE_INSTR  : std_logic_vector(2 downto 0) := "001";
	constant cCPY_INSTR    : std_logic_vector(2 downto 0) := "010";
	constant cJMP_INSTR    : std_logic_vector(2 downto 0) := "011";
	constant cBR_INSTR     : std_logic_vector(2 downto 0) := "100";
	constant cTEST_INSTR   : std_logic_vector(2 downto 0) := "101";
	constant cMAC_INSTR    : std_logic_vector(2 downto 0) := "110";
	constant cMOD2_INSTR   : std_logic_vector(2 downto 0) := "111";

   --READ WRITE constant for memory
	constant cREAD  : std_logic := '1';
	constant cWRITE : std_logic := '0';
	
	component proc is
		port (
			clk_i   : in std_logic;
			reset_i : in std_logic;
			
			addr_bus_p_o : out std_logic_vector(10 downto 0);
			data_bus_p_i : in std_logic_vector(15 downto 0);
			
			addr_bus_d_o  : out std_logic_vector(10 downto 0);
			data_bus_d_io : inout std_logic_vector(7 downto 0);
			
			rwmem_o : out std_logic
		);
	end component;

	component controler
		port (
			clk_i   : in std_logic;
			reset_i : in std_logic;
			
			flag_i   : in std_logic;
			opcode_i : in std_logic_vector(2 downto 0);
			
			rwmem_o : out std_logic;
			
			selaa_o : out std_logic;
			rwreg_o : out std_logic;
			opalu_o : out std_logic_vector(2 downto 0);
			inalu_o : out std_logic;
			inreg_o : out std_logic;
			oealu_o : out std_logic;
			ldres_o : out std_logic;
			ldpc_o  : out std_logic;
			selpc_o : out std_logic;
			ldir_o  : out std_logic
		);
	end component;
	
	component traitement
		port (
			clk_i   : in std_logic;
			reset_i : in std_logic;
			
			selaa_i : in std_logic;
			rwreg_i : in std_logic;
			opalu_i : in std_logic_vector(2 downto 0);
			inalu_i : in std_logic;
			inreg_i : in std_logic;
			oealu_i : in std_logic;
			ldres_i : in std_logic;
			ldpc_i  : in std_logic;
			selpc_i : in std_logic;
			ldir_i  : in std_logic;
			
			flag_o   : out std_logic;
			opcode_o : out std_logic_vector(2 downto 0);
			
			addr_bus_p_o : out std_logic_vector(10 downto 0);
			data_bus_p_i : in std_logic_vector(15 downto 0);
			
			addr_bus_d_o  : out std_logic_vector(10 downto 0);
			data_bus_d_io : inout std_logic_vector(7 downto 0)
		);
	end component;

	component registers
		port (
			clk_i   : in std_logic;
			reset_i : in std_logic;
			
			input_bus_i : in std_logic_vector(7 downto 0);
			
			a_o : out std_logic_vector(7 downto 0);
			b_o : out std_logic_vector(7 downto 0);
			
			w_i : in std_logic_vector(1 downto 0);
			reg2_i : in std_logic_vector(1 downto 0);
			outB_i : in std_logic_vector(1 downto 0); 
			
			rw_i  : in std_logic;
			selaa_i : in std_logic         
		);
	end component;
	
	component pc
		port (
			clk_i   : in std_logic;
			reset_i : in std_logic;
			
			ldpc_i   : in std_logic;
			selpc_i  : in std_logic;
			
			jmp_i : in std_logic_vector(11 downto 0);
			pc_o : out std_logic_vector(10 downto 0)
		);
	end component;
	
	component ir
		port (
			clk_i : in std_logic;
			reset_i : in std_logic;

			ldir_i : in std_logic;
			ir_i : in std_logic_vector(15 downto 0);
			ir_o : out std_logic_vector(15 downto 0)
		);
	end component;

	component alu
		port (
			clk_i   : in std_logic;
			reset_i : in std_logic;
			
			inalu_i  : in std_logic;
			opalu_i  : in std_logic_vector(2 downto 0);
			ldres_i  : in std_logic;
			
			a_i      : in std_logic_vector(7 downto 0);
			b_i      : in std_logic_vector(7 downto 0);
			
			carry_o  : out std_logic;
			flag_o   : out std_logic;
			result_o : out std_logic_vector(7 downto 0)
		);
	end component;

	component write_back 
		port (
			data_i : in std_logic_vector(7 downto 0);
			
			oealu_i : in std_logic;
			inreg_i : in std_logic;
			
			data2mem_io : inout std_logic_vector(7 downto 0);
			data2alu_o : out std_logic_vector(7 downto 0)
		);
	end component;

	component data_mem
		port (
			clk_i : in std_logic;
			adr_i : in std_logic_vector(10 downto 0);
			data_io : inout std_logic_vector(7 downto 0);
			ce_i : in std_logic;
			rw_i : in std_logic
		);
	end component;
	
	component prog_mem
		port (
			clk_i : in std_logic;
			adr_i : in std_logic_vector(10 downto 0);
			data_io : inout std_logic_vector(15 downto 0);
			ce_i : in std_logic;
			rw_i : in std_logic
		);
	end component;

end miniproc_pkg;

package body miniproc_pkg is

---- Example 1
--  function <function_name>  (signal <signal_name> : in <type_declaration>  ) return <type_declaration> is
--    variable <variable_name>     : <type_declaration>;
--  begin
--    <variable_name> := <signal_name> xor <signal_name>;
--    return <variable_name>; 
--  end <function_name>;

---- Example 2
--  function <function_name>  (signal <signal_name> : in <type_declaration>;
--                         signal <signal_name>   : in <type_declaration>  ) return <type_declaration> is
--  begin
--    if (<signal_name> = '1') then
--      return <signal_name>;
--    else
--      return 'Z';
--    end if;
--  end <function_name>;

---- Procedure Example
--  procedure <procedure_name>  (<type_declaration> <constant_name>  : in <type_declaration>) is
--    
--  begin
--    
--  end <procedure_name>;
 
end miniproc_pkg;
