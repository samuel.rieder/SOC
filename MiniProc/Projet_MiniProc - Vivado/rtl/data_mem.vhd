----------------------------------------------------------------------------------
-- Company: HEIA-FR
-- Engineer: Daniel Oberson
-- 
-- Create Date:    14:37:36 11/02/2015 
-- Design Name: 
-- Module Name:    data_mem - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.miniproc_pkg.all;

entity data_mem is
	port (
		clk_i : in std_logic;
		adr_i : in std_logic_vector(10 downto 0);
		data_io : inout std_logic_vector(7 downto 0);
		ce_i : in std_logic;
		rw_i : in std_logic
	);
end data_mem;

architecture synt of data_mem is

	type data_mem_type is array(31 downto 0) of std_logic_vector(7 downto 0);
	--data memory
	signal data_mem_s : data_mem_type := ( 
		0 => "00000111", 
		1 => "00000100",
		3 => "00000001",
		others => "00000000"
	);
	signal adrint : std_logic_vector(4 downto 0);
	signal outint : std_logic_vector(7 downto 0);
	signal ceint  : std_logic;
	
begin
	adrint <= adr_i(4 downto 0);
	ceint <= '1' when (ce_i='1' and to_integer(unsigned(adr_i))<=31) else '0';
	
	process (clk_i)
	begin
		if falling_edge(clk_i) then
			outint <= data_mem_s(to_integer(unsigned(adrint)));
			if rw_i=cWRITE then
				if ceint='1' then
					data_mem_s(to_integer(unsigned(adrint))) <= data_io;
				end if;
			end if;
		end if;
	end process;
	
	data_io <= (others => 'Z') when (ceint='1' and rw_i=cWRITE) else outint;
	
end synt;