----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    14:23:10 02/05/2018 
-- Design Name: 
-- Module Name:    write_back - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity write_back is
	port (
		data_i : in std_logic_vector(7 downto 0);
		
		oealu_i : in std_logic;
		inreg_i : in std_logic;
		
		data2mem_io : inout std_logic_vector(7 downto 0);
		data2alu_o : out std_logic_vector(7 downto 0)
	);
end write_back;

architecture Behavioral of write_back is
    begin
	data2mem_io <= data_i when (oealu_i = '1') else (others =>'Z');
    data2alu_o <=  data2mem_io when (inreg_i='1') else data_i;
end Behavioral;

