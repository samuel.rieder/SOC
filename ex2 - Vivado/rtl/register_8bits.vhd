----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:59:58 02/25/2016 
-- Design Name: 
-- Module Name:    register_8bits - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity register_8bits is
	port (
		clk_i   : in std_logic;
		reset_i : in std_logic;
		
		load_i : in std_logic;
		port_i : in std_logic_vector (7 downto 0);
		port_o : out std_logic_vector (7 downto 0)
	);
end register_8bits;

architecture Behavioral of register_8bits is

begin

	process (clk_i,reset_i)
	begin
		if rising_edge(clk_i) then
			if (reset_i='1') then
				port_o <= (others=>'0');
			else
				if (load_i='1') then
					port_o <= port_i;
				end if;
			end if;
		end if;
	end process;
	
end Behavioral;

