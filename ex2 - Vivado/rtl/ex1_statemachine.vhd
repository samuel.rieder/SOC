----------------------------------------------------------------------------------
-- Company: HEIA-FR
-- Engineer: Daniel Oberson
-- 
-- Create Date:    13:52:55 02/25/2016 
-- Design Name: 
-- Module Name:    ex1_statemachine - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.two_s_complement_pkg.all;

entity ex1_statemachine is
	port ( 
		clk_i   : in std_logic;
		reset_i : in std_logic;
		
		start_i : in std_logic;
		port_i  : in std_logic_vector (7 downto 0);
		
		rdy_o   : out std_logic;
		port_o  : out std_logic_vector(7 downto 0)
	);
end ex1_statemachine;

architecture Behavioral of ex1_statemachine is

	--Signal
	signal load_in_s : std_logic;
	signal port_in_s : std_logic_vector (7 downto 0);	
	
	signal load_out_s : std_logic;
	signal port_out_s : std_logic_vector (7 downto 0);
	
begin

	--Input register 8 bits
	cmp_in_register : register_8bits 
		port map(
			clk_i   => clk_i,
			reset_i => reset_i,
			
			load_i => load_in_s,
			port_i => port_i,
			port_o => port_in_s
		);

	--State Machine
	cmp_statemachine : statemachine
		port map( 
			clk_i   => clk_i,
			reset_i => reset_i,
			
			start_i => start_i,
			port_i  => port_in_s,
			
			rdy_o      => rdy_o,
			load_in_o  => load_in_s,
			load_out_o => load_out_s,
			
			port_o  => port_out_s
		);
	
	--Output register 8 bits
	cmp_out_register : register_8bits 
		port map(
			clk_i   => clk_i,
			reset_i => reset_i,
			
			load_i => load_out_s,
			port_i => port_out_s,
			port_o => port_o
		);

end Behavioral;

