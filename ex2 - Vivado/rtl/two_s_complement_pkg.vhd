--
--	Package for two's complement
--
--	Purpose: This package contains:
--           - the decleration of the register_8bits and statemachine components
--           - the function complement2()
--


library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;

package two_s_complement_pkg is
--Component
component register_8bits 
    port (
        clk_i   : in std_logic;
        reset_i : in std_logic;
        
        load_i : in std_logic;
        port_i : in std_logic_vector (7 downto 0);
        port_o : out std_logic_vector (7 downto 0)
    );
end component;

component statemachine 
    port ( 
        clk_i   : in std_logic;
        reset_i : in std_logic;
        
        start_i : in std_logic;
        port_i  : in std_logic_vector (7 downto 0);
        
        rdy_o      : out std_logic;
        load_in_o  : out std_logic;
        load_out_o : out std_logic;
        
        port_o  : out std_logic_vector(7 downto 0)
    );
end component;
-- Declare functions and procedure
--
 function complement2 (
    signal input_i : in std_logic_vector(7 downto 0))
    return std_logic_vector;
-- procedure <procedure_name> (<type_declaration> <constant_name>	: in <type_declaration>);
--

end two_s_complement_pkg;

package body two_s_complement_pkg is

---- function body
  function complement2  (signal input_i : in std_logic_vector(7 downto 0)
                         ) return std_logic_vector is
  begin
    return std_logic_vector(unsigned(not input_i)+1);
  end complement2;
 
end two_s_complement_pkg;
