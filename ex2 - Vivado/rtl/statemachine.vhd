----------------------------------------------------------------------------------
-- Company:        HEIA-FR
-- Engineer:       Daniel Oberson, Samuel Rieder
-- 
-- Create Date:    16:36:11 02/25/2016 
-- Design Name: 
-- Module Name:    statemachine - Behavioral 
-- Project Name:   SOPC Miniproc
-- Target Devices: ZedBoard
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Revision 0.1  - Solution of the ex1
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.two_s_complement_pkg.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity statemachine is
	port ( 
		clk_i   : in std_logic;
		reset_i : in std_logic;
		
		start_i : in std_logic;
		port_i  : in std_logic_vector (7 downto 0);
		
		rdy_o      : out std_logic;
		load_in_o  : out std_logic;
		load_out_o : out std_logic;
		
		port_o  : out std_logic_vector(7 downto 0)
	);
end statemachine;

architecture Behavioral of statemachine is

	--Type definition of the type state_t
    type state_t is
    (
        IDLE,
        READ_IN,
        WRITE_OUT
    );	
	--Signal
    signal state: state_t; --to use as state signal in the FSM
begin
	--State machine - next state
    reg_1: process(clk_i, reset_i)
        begin
            if reset_i = '1' then
                state <= IDLE;
            elsif rising_edge(clk_i) then
                case state is
                    when IDLE =>
                        if start_i = '1' then
                            state <= READ_IN;
                        end if;
                    when READ_IN =>
                        state <= WRITE_OUT;    
                    when WRITE_OUT =>
                        state <= IDLE;
                    when others =>
                        state <= IDLE;
                end case;
            end if;     
    end process;
	--Output of state machine
	reg_2: process(state)
	   begin
	       case state is
	           when IDLE =>
	               rdy_o      <= '1';
	               load_in_o  <= '0';
	               load_out_o <= '0';
	           when READ_IN =>
                   rdy_o      <= '0';
                   load_in_o  <= '1';
                   load_out_o <= '0';
	           when WRITE_OUT =>
                   rdy_o      <= '0';
                   load_in_o  <= '0';
                   load_out_o <= '1';
	       end case;
	end process;
	--Comnbinatory 2's complement (combinatory)
    port_o <= complement2(port_i); -- two's complement
end Behavioral;

